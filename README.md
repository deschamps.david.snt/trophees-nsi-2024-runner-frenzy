# Trophées NSI 2024 - Runner Frenzy

## Membres de l'équipe
* Novak Sebastian
* Clément Dujardin Duribreux
* Florent Dujardin Duribreux

## Établissement scolaire
Lycée Gustave Eiffel
59280 Armentières 

## Enseignant
M. MABRIEZ Nathan

## Résumé de votre projet (500 caractères max)
Runner Frenzy est un projet de jeu « runner » en 2D inspiré de Subway Surfers. Le but du jeu est d'éviter les obstacles que le personnage rencontrera. Le personnage ira de plus en plus vite ce qui rendra les choses beaucoup plus difficiles. Avez-vous ce qu'il faut pour devenir le meilleur runner ?

## Vidéo de démonstration
La vidéo du projet est disponible sur ce lien : [Vidéo de démonstration](https://tube-numerique-educatif.apps.education.fr/w/gVLUcaEWEPNsmbkLqUWJJk)

## Protocole d'utilisation
Pour demarrer le jeu, il suffit d'exécuter les commandes suivantes : 
```bash
pip3 install -r requirements.txt
cd sources
python3 src/main.py
```
Pour lancer le serveur Flask, il faut exécuter la commande :
```bash
cd sources
python3 src/Web/Serveur.py
```
Ensuite le site web est à l'adresse suivante : 127.0.0.1:2000/home

