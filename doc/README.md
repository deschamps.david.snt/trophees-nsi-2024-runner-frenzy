# Documentation du projet

Le projet est divisé en une multitude de classes, chacune avec son propre fichier ``.py``
Voici l'utilité de chaque classe du jeu :
- ``Button`` : Cette classe gère l'utilisation et l'affichage des boutons des menus et de l'écran de game over.
- ``DeathMenu`` : Cette classe représente l'écran de Game Over.
- ``Entity`` : Cette classe s'occupe de l'appartion des sprites du personnage et des obstacles.
- ``File`` : Cette classe utilise le principe de la file vu en cours de NSI.
- ``Pile`` : Cette classe utilise le principe de la pile vu en cours de NSI.
- ``Game`` : La classe Game s'occupe des genéralités du jeu comme les touches, le score et autres.
- ``Graphe`` : La classe Graphe utilise le principe des graphes vu en cours de NSI.
- ``GrapheMenu`` : GrapheMenu gère tous les menus du jeu et permet le changement des menus dans le jeu.
- ``Img`` : Img charge les sprites du jeu
- ``MainMenu`` : MainMenu est la classe qui représente le menu principal.
- ``Map`` : Map s'occupe des collisions dans le jeu et du positionnement des obstacles et du personnage sur la carte.
- ``Menu`` : Menu donne les caractéristiques esthétiques du menu.
- ``Obstacle_Area`` : Cette classe génère les obstacles et les efface de la carte quand le personnage les a passés.
- ``Obstacle`` : Obstacle créé les objets « obstacles »
- ``OptionMenu`` : OptionMenu est la classe qui représente le menu d'options du jeu.
- ``OPTIONS`` : Cette classe est un comme un fichier de configuration du jeu montrant les paramètres du jeu.
- ``PauseMenu`` : PauseMenu est la classe qui représente le menu de pause du jeu.
- ``Player`` : Player s'occupe du personnage, donnant son mouvement, ses animations, sa position et la vitesse de son déplacement.
- ``SceneMenu`` : SceneMenu est une classe qui représente la scene du jeu
- ``Sql`` : Sql se charge de gestionner la base de données contenant les scores des joueurs. Cette classe ajoute des nouveaux scores ou les extrait, et permet au joueur de se connecter à la base de données pour pouvoir sauvegarder son score de la partie actuelle sous son nom.
- ``Window`` : Cette classe crée une fenêtre où le jeu s'affichera.
- ``WindowConnection`` : Cette classe crée la fenêtre où le joueur peut se connecter à la base de données.